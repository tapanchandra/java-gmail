package com.lilaccorp.learning.java;

import com.google.api.services.gmail.GmailScopes;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class GmailQuickStart
{
    
    public static void main( String[] args )
    {
        GmailReader reader = null;
        Map<String,String> msgs = null;

        try {
            reader = new GmailReader();
            msgs = reader.readEmails(0,"me");

        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        for (String historyId: msgs.keySet()) {
            System.out.println(historyId + ":" + msgs.get(historyId));
        }
    }
}
