package com.lilaccorp.learning.java;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.json.webtoken.JsonWebToken;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.google.api.services.gmail.model.History;
import com.google.api.services.gmail.model.ListHistoryResponse;

import java.io.*;
import java.math.BigInteger;
import java.security.GeneralSecurityException;
import java.util.*;

/**
 * Created by priya on 7/23/2016.
 */
public class GmailReader {

    JsonFactory JSON_FACTORY = null;
    HttpTransport httpTransport = null;
    private String APPLICATION_NAME = "Java Gmail Access";

    public GmailReader() throws GeneralSecurityException, IOException {
        JSON_FACTORY = JacksonFactory.getDefaultInstance();
        httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    }

    private Credential authorize() throws IOException {

        File a = new File("asdfasdfa.txt");
        a.createNewFile();

        GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream("src/main/resources/credentials/My Project-774f05f16a82.json"))
                .createScoped(Collections.singleton(GmailScopes.GMAIL_READONLY));

        return credential;
    }

    private Gmail getGmailService() throws IOException {
        Credential credential = this.authorize();
        return new Gmail.Builder(httpTransport, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME)
                .build();
    }


    public Map<String,String> readEmails(String startId,String userId) throws IOException {

        Map<String,String> messages = new HashMap<String, String>();
        List<History> histories = new ArrayList<History>();

        Gmail service = this.getGmailService();


       // Gmail.Users.History.List listRequest = service.users().history().list(userId);

        //if(startId != "0")
            //listRequest = listRequest.setStartHistoryId(new BigInteger(startId));

        ListHistoryResponse response = service.users().history().list(userId).execute();
        histories.addAll(response.getHistory());


        for(History h:histories){
            messages.put(String.valueOf(h.getId()),h.toPrettyString());
        }

        return messages;
    }
}
